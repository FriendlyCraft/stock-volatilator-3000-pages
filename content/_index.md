# Stock Vollatilator 3000
![Pink Raging Wojak](https://gitlab.com/FriendlyCraft/stock-volatilator-3000-pages/-/raw/master/static/pinkwoj.png)

---

**Table of contents**
* Short Overview
* Architecture Drawing

### Short Overview
Trading stocks has become more and more popular every day, but regardless of popularity, it still is a risky game. Making trading safe was never an option, but assessing the risk was always possible. Therefore, **this.team** decided to help people assess risks. To achieve this goal, we created **Stock-Volatilator-3000**, which calculates the volatility of a specified stock in a specified interval of time. 

The volatility of a stock is calculated using the standard deviation formula. 

![Formula](https://www.gstatic.com/education/formulas/images_long_sheet/en/population_standard_deviation.svg)

It provides information about stock stability. Higher the volatility, the higher the amount of money that can be earned or lost.

---

The following project is created in java and uses the following frameworks/APIs:

- Spring Boot
- Mockito
- Swagger
- Alpha Vantage


To discover the problems and ensure the correctness of the output, API has been tested using several testing methods like Unit, Mocked and Integration tests. Other Types of tests are planned for the future.

### Architecture Drawing

![Architecture](https://gitlab.com/FriendlyCraft/stock-volatilator-3000-pages/-/raw/master/static/Architecture.png)

### Technical Description

Our solution follows RestAPI principles. It has one main and two side controllers. All of them are accessible on **Base-URL/{controler_name}**, or using Swagger UI. 

#### Volatility Controller

Our main (Volatility) controller accepts three variables (start date, end date, company symbol) and returns JSON with volatility and used entries. To accomplish the following task, our controller uses three services. The first (Volatility) service validates the parameters, by using **Response Utility**. Response Utility ensures the following:
- Company symbol exists
- The date range is not longer than 100 days
- dates are not from the future
- start date is before the end date

Afterward, passes the parameters to the AlphaVantage service, which using HTTP get request queries data with entered parameters and sends HTTP Reply back to the **Volatility Service**. The reply is filtered according to necessary dates, transformed, and sent to **VolatilityCalculator**.

Inside of the **VolatlityCalculator**, data is extracted from JSON into Array, and using stream functions in **Deviation Utility** volatility is calculated. Volatility is appended to JSON and sent back to the **Volatility Controller**. The output is visible in Swagger UI.

#### Custom Error Controller
The task of this controller is relatively simple. It returns custom documented errors.

#### Index Controller
Ensures that API is up.

All of the following services and controllers filter the input.




